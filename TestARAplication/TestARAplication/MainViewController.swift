//
//  MainViewController.swift
//  TestARAplication
//
//  Created by Ivan Zagorulko on 27.07.2018.
//  Copyright © 2018 Patronus IT. All rights reserved.
//

import UIKit

class MainViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.loadFbxModels()
    }
    
    func createFBXDirectory() {
        let documentsDirectory = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]
        let filePath = documentsDirectory.appendingPathComponent("FBX Models")
        if !FileManager.default.fileExists(atPath: filePath.path) {
            do {
                try FileManager.default.createDirectory(atPath: filePath.path, withIntermediateDirectories: true, attributes: nil)
            } catch let error as NSError {
                print("Error creating directory: \(error.localizedDescription)")
            }
        }
    }
    
    func loadFbxModels() {
        self.createFBXDirectory()
        let bundlePath = Bundle.main.path(forResource: "FBX", ofType: nil)
        let documentsDirectory = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]
        let filePath = documentsDirectory.appendingPathComponent("FBX Models")
        if FileManager.default.fileExists(atPath: filePath.path) {
            let files = try! FileManager.default.contentsOfDirectory(atPath: bundlePath!)
            for file in files {
                let fullDestPath = filePath.appendingPathComponent(file)
                if !FileManager.default.fileExists(atPath: fullDestPath.path) {
                    try! FileManager.default.copyItem(atPath: "\(bundlePath!)/\(file)", toPath: fullDestPath.path)
                }
            }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func browseFileAction(_ sender: Any) {
        let fileBr = FileBrowser()
        self.present(fileBr, animated: true, completion: nil)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
