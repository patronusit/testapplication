//
//  LoadManager.swift
//  TestARAplication
//
//  Created by Ivan Zagorulko on 27.07.2018.
//  Copyright © 2018 Patronus IT. All rights reserved.
//

import UIKit

protocol LoadMangerDelegate {
    func finishDownloadingWith(path: URL)
    func completeWithError(error: Error?)
    func writeFileData(bytesWritten: Int64, totalBytesWritten: Int64, totalBytesExpectedToWrite: Int64)
}

class LoadManager: NSObject, URLSessionDownloadDelegate {
    
    static let sharedManager = LoadManager()

    var delegate: LoadMangerDelegate?
    
    var downloadTask: URLSessionDownloadTask!
    var backgroundSession: URLSession!
    var fileNameString: String!
    var remoteURL: URL! {
        didSet {
            fileNameString = remoteURL?.lastPathComponent
        }
    }
    var destinationURLForFile: URL!
    var directoryPath: String! {
        didSet {
            destinationURLForFile = URL(fileURLWithPath: directoryPath.appendingFormat("/\(fileNameString)"))
        }
    }
    
    override init() {
        super.init()
        let backgroundSessionConfiguration = URLSessionConfiguration.background(withIdentifier: "backgroundSession")
        backgroundSession = Foundation.URLSession(configuration: backgroundSessionConfiguration, delegate: self, delegateQueue: OperationQueue.main)
    }
    
    public func startDownload(fromURL: URL) {
        directoryPath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0]
        
        // Get the original file name from the original request.
        remoteURL = fromURL
        downloadTask = backgroundSession.downloadTask(with: remoteURL!)
        downloadTask.resume()
    }
    
    public func pauseDownload() {
        guard downloadTask != nil else { return }
        downloadTask.suspend()
    }
    
    public func resumeDownload() {
        guard downloadTask != nil else { return }
        downloadTask.resume()
    }
    
    public func cancelDownload() {
        guard downloadTask != nil else { return }
        downloadTask.cancel()
        deleteFile()
    }
    
    func deleteFile() {
        do {
            try FileManager.default.removeItem(at: URL(fileURLWithPath: directoryPath.appendingFormat("/\(fileNameString!)")))
        } catch (let writeError) {
            print("Error removing a file : \(writeError)")
        }
    }
    
    // MARK: - URLSessionDownloadDelegate
    
    func urlSession(_ session: URLSession, downloadTask: URLSessionDownloadTask, didFinishDownloadingTo location: URL) {
        
        let destinationURLForFile = URL(fileURLWithPath: directoryPath.appendingFormat("/\(fileNameString!)"))
        if FileManager.default.fileExists(atPath: destinationURLForFile.path){
            delegate?.finishDownloadingWith(path: destinationURLForFile)
        }
        else{
            do {
                try FileManager.default.moveItem(at: location, to: destinationURLForFile)
                delegate?.finishDownloadingWith(path: destinationURLForFile)
            }catch{
                print("An error occurred while moving file to destination url")
            }
        }
    }
    
    func urlSession(_ session: URLSession, downloadTask: URLSessionDownloadTask, didWriteData bytesWritten: Int64, totalBytesWritten: Int64, totalBytesExpectedToWrite: Int64) {
        print("\(totalBytesWritten) bytes of \(totalBytesExpectedToWrite) are downloaded")
        delegate?.writeFileData(bytesWritten: bytesWritten, totalBytesWritten: totalBytesWritten, totalBytesExpectedToWrite: totalBytesExpectedToWrite)
    }
    
    // MARK: - URLSessionTaskDelegate
    
    func urlSession(_ session: URLSession, task: URLSessionTask, didCompleteWithError error: Error?) {
        downloadTask = nil
        delegate?.completeWithError(error: error)
        if (error != nil) {
            print(error!.localizedDescription)
        }else{
            print("The task finished transferring data successfully")
        }
    }
}
