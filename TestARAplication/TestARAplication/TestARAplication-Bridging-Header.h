//
//  TestARAplication-Bridging-Header.h
//  TestARAplication
//
//  Created by Ivan Zagorulko on 30.07.2018.
//  Copyright © 2018 Patronus IT. All rights reserved.
//

#import <AssimpKit/AssimpImporter.h>
#import <AssimpKit/SCNAssimpAnimation.h>
#import <AssimpKit/SCNAssimpScene.h>
#import <AssimpKit/SCNAssimpAnimSettings.h>
#import <AssimpKit/SCNScene+AssimpImport.h>
#import <AssimpKit/SCNNode+AssimpImport.h>
#import <AssimpKit/PostProcessingFlags.h>
