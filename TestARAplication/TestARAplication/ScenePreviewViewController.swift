//
//  ScenePreviewViewController.swift
//  TestARAplication
//
//  Created by Ivan Zagorulko on 31.07.2018.
//  Copyright © 2018 Patronus IT. All rights reserved.
//

import UIKit
import SceneKit

class ScenePreviewViewController: UIViewController, CAAnimationDelegate {
    
    @IBOutlet weak var activityIndicatorView: UIActivityIndicatorView!
    @IBOutlet var sceneView: SCNView!
    var scene = SCNScene()
    
    var file: FBFile? {
        didSet {
            self.title = file?.displayName
        }
    }
    
    var modelContainerNode: SCNNode = {
        let modelContainerNode = SCNNode()
        modelContainerNode.name = "Model Container Node"
        modelContainerNode.constraints = []
        return modelContainerNode
    }()

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        sceneView.allowsCameraControl = true
        sceneView.autoenablesDefaultLighting = true
        sceneView.scene = scene
        sceneView.backgroundColor = UIColor.black
        sceneView.showsStatistics = true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.activityIndicatorView.startAnimating()
        DispatchQueue.main.async {
            self.showModelScenPreview()
            DispatchQueue.main.async {
                self.activityIndicatorView.stopAnimating()
            }
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        self.unloadScene()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func showModelScenPreview() {
        guard let file = file else { return }
        let filePath = file.filePath.path
        
        DispatchQueue.main.async {
            if (filePath as NSString).pathExtension == "scn" {
                do {
                    let scnScene = try SCNScene(url: URL(fileURLWithPath: filePath), options: nil)
                    for childNode in (scnScene.rootNode.childNodes) {
                        self.modelContainerNode.addChildNode(childNode)
                    }
                } catch let error {
                    print(error)
                }

            } else {
                let animScene = try! AssimpImporter().importScene(filePath, postProcessFlags: AssimpKitPostProcessSteps(rawValue: AssimpKitPostProcessSteps.process_FlipUVs.rawValue | AssimpKitPostProcessSteps.process_Triangulate.rawValue ), error: ())
                if animScene != nil {
                    if let modelScene = animScene.modelScene {
                        for childNode in modelScene.rootNode.childNodes {
                            self.modelContainerNode.addChildNode(childNode)
                        }
                    }
                    let animationKeys = animScene.animationKeys()
                    // If multiple animations exist, load the first animation
                    if let numberOfAnimationKeys = animationKeys?.count {
                        if numberOfAnimationKeys > 0 {
                            let settings = SCNAssimpAnimSettings()
                            settings.repeatCount = 5
                            
                            let key = animationKeys![0] as! String
                            let eventBlock: SCNAnimationEventBlock = { animation, animatedObject, playingBackwards in
                                print("Animation Event triggered")
                                return
                            }
                            let animEvent = SCNAnimationEvent(keyTime: 0.1, block: eventBlock)
                            let animEvents: [SCNAnimationEvent]  = [animEvent]
                            settings.animationEvents = animEvents
                            settings.delegate = self
                            
                            if let animation = animScene.animationScene(forKey: key) {
                                self.modelContainerNode.addAnimationScene(animation, forKey: key, with: settings)
                            }
                        }
                    }
                    
                }
            }
        }
        sceneView.scene?.rootNode.addChildNode(self.modelContainerNode)
    }
    
    func unloadScene() {
        for node in self.scene.rootNode.childNodes {
            node.removeFromParentNode()
            node.removeAllAnimations()
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
