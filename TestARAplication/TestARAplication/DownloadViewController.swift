//
//  DownloadViewController.swift
//  TestARAplication
//
//  Created by Ivan Zagorulko on 27.07.2018.
//  Copyright © 2018 Patronus IT. All rights reserved.
//

import UIKit
import SceneKit

class DownloadViewController: UIViewController, LoadMangerDelegate {
    
    @IBOutlet weak var startDownloadButton: UIButton!
    @IBOutlet weak var pauseDownloadButton: UIButton!
    @IBOutlet weak var cancelDownloadButton: UIButton!
    
    @IBOutlet weak var urlTextField: UITextField!
    @IBOutlet weak var statusLabel: UILabel!
    @IBOutlet weak var statusDownloadLabel: UILabel!
    @IBOutlet weak var fileNameLabel: UILabel!
    @IBOutlet weak var fileDownloadProgressView: UIProgressView!
    
    let downloadManager = LoadManager.sharedManager
    var isStartDownload = false
    var isPauseDownload = false
    var isFinishDownload = false
    var fileName = "FileName"
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        fileDownloadProgressView.setProgress(0.0, animated: true)
        downloadManager.delegate = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if self.isFinishDownload {
            self.dismiss(animated: true, completion: nil)
        } else {
            self.checkPastString()
        }
    }
    
    func checkPastString() {
        if let pastString = UIPasteboard.general.string {
            let pathExtension = (pastString as NSString).pathExtension
            if (SCNScene.canImportFileExtension(pathExtension) || pathExtension == "zip" || pathExtension == "scn") {
                statusLabel.text = "URL is copied. Click Download"
                self.urlTextField.text = pastString
            } else {
                statusLabel.text = "Copied unsupported URL"
            }
        } else {
            statusLabel.text = "Enter URL or Copy in Safari"
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func showFileBrowser() {
        let fileBr = FileBrowser()
        self.present(fileBr, animated: true, completion: nil)
    }
    
    @IBAction func closeAction(_ sender: Any) {
        if isStartDownload {
            let alertController = UIAlertController(title: "Cancel Download", message: "Do you want to cancel download file?", preferredStyle: .alert)
            alertController.addAction(.init(title: "Yes", style: .default, handler: { (action) in
                self.downloadManager.cancelDownload()
                self.dismiss(animated: true, completion: nil)
            }))
        } else {
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    @IBAction func startDownloadAction(_ sender: Any) {
        let urlString = self.urlTextField.text
        guard urlString != nil && urlString?.trimmingCharacters(in: .whitespacesAndNewlines) != "" else { return }
        let url = URL(string: urlString!)
        self.fileName = url!.lastPathComponent
        self.fileNameLabel.text = "\(self.fileName) - 0.0%"
        isStartDownload = true
        self.statusDownloadLabel.text = "Downloading..."
        downloadManager.startDownload(fromURL: url!)
    }
    
    @IBAction func pauseDownloadAction(_ sender: Any) {
        guard isStartDownload == true else { return }
        if isPauseDownload == false {
            isPauseDownload = true
            downloadManager.pauseDownload()
            self.pauseDownloadButton.setTitle("Resume", for: .normal)
        } else {
            isPauseDownload = false
            downloadManager.resumeDownload()
            self.pauseDownloadButton.setTitle("Pause", for: .normal)
        }
    }
    
    @IBAction func cancelDownloadAction(_ sender: Any) {
        downloadManager.cancelDownload()
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    //MARK - LoadMangerDelegate
    
    func finishDownloadingWith(path: URL) {
        self.isStartDownload = false
        if path.pathExtension == "zip" {
            self.statusDownloadLabel.text = "Unzipping..."
            let archiveURL = URL(fileURLWithPath: path.path)
            let destinationURL = URL(fileURLWithPath: path.deletingPathExtension().path)
            guard let archive = Zipper(url: archiveURL, accessMode: .read) else  { return }
            do {
                try archive.unzip(to: destinationURL)
            } catch _ {}
        }
        self.isFinishDownload = true
        self.showFileBrowser()
    }
    
    func completeWithError(error: Error?) {
        fileDownloadProgressView.setProgress(0.0, animated: true)
    }
    
    func writeFileData(bytesWritten: Int64, totalBytesWritten: Int64, totalBytesExpectedToWrite: Int64) {
        let downloadProgress = Float(totalBytesWritten)/Float(totalBytesExpectedToWrite)
        self.fileNameLabel.text = String(format: "%@ - %.1f%%", self.fileName, downloadProgress * 100)
        fileDownloadProgressView.setProgress(downloadProgress, animated: true)
    }

}
