/*
See LICENSE folder for this sample’s licensing information.

Abstract:
Methods on the main view controller for handling virtual object loading and movement
*/

import UIKit
import ARKit

extension ViewController: VirtualObjectSelectionViewControllerDelegate {
    
    func setNewVirtualObjectPosition(_ pos: SCNVector3) {
        guard let object = virtualObject, let cameraTransform = session.currentFrame?.camera.transform else {
            return
        }
        recentVirtualObjectDistances.removeAll()
        let cameraWorldPos = SCNVector3.positionFromTransform(cameraTransform)
        var cameraToPosition = pos - cameraWorldPos
        
        // Limit the distance of the object from the camera to a maximum of 10 meters.
        cameraToPosition.setMaximumLength(10)
        object.position = cameraWorldPos + cameraToPosition
        if object.parent == nil {
            sceneView.scene.rootNode.addChildNode(object)
        }
    }
    
    func resetVirtualObject() {
        virtualObject?.unloadModel()
        virtualObject?.removeFromParentNode()
        virtualObject = nil
        
        DispatchQueue.main.async {
            self.hideObjectLoadingUI()
        }
        
        // Reset selected object id for row highlighting in object selection view controller.
        UserDefaults.standard.set(-1, forKey: "selectedRowIndex")
    }
    
    func loadVirtualObject(at index: Int) {
        resetVirtualObject()
        // Load the content asynchronously.
        displayObjectLoadingUI()
        DispatchQueue.global().async {
            let object = VirtualObject.availableObjects[index]
            self.virtualObject = object
            let loadIsSuccessful = object.loadModel()
            DispatchQueue.main.async {
                if !loadIsSuccessful {
                    // Remove progress indicator
                    self.resetVirtualObject()
                } else {
                    // Immediately place the object in 3D space.
                    if let lastFocusSquarePos = self.focusSquare.lastPosition {
                        self.setNewVirtualObjectPosition(lastFocusSquarePos)
                    } else {
                        self.setNewVirtualObjectPosition(SCNVector3Zero)
                    }
                    self.hideObjectLoadingUI()
                }
            }
        }
    }
    
    // MARK: - VirtualObjectSelectionViewControllerDelegate
    
    func virtualObjectSelectionViewController(_: VirtualObjectSelectionViewController, didSelectObjectAt index: Int) {
        loadVirtualObject(at: index)
    }
    
    func virtualObjectSelectionViewControllerDidDeselectObject(_: VirtualObjectSelectionViewController) {
        resetVirtualObject()
    }

    // MARK: Object Loading UI

    func displayObjectLoadingUI() {
        // Show progress indicator.
        spinner.startAnimating()
        
        addObjectButton.setImage(#imageLiteral(resourceName: "buttonring"), for: [])

        addObjectButton.isEnabled = false
        isRestartAvailable = false
    }

    func hideObjectLoadingUI() {
        // Hide progress indicator.
        spinner.stopAnimating()

        addObjectButton.setImage(#imageLiteral(resourceName: "add"), for: [])
        addObjectButton.setImage(#imageLiteral(resourceName: "addPressed"), for: [.highlighted])

        addObjectButton.isEnabled = true
        isRestartAvailable = true
    }
}
